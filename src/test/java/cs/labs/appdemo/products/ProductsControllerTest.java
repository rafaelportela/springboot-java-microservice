package cs.labs.appdemo.products;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ProductsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void createAndFindProductById() throws Exception {

        mockMvc.perform(post("/products")
                        .content("{\"id\":\"10\",\"name\":\"desk\",\"price\":\"10.00\"}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        MockHttpServletResponse response = mockMvc
                .perform(get("/products/10"))
                .andExpect(status().isOk())
                .andReturn().getResponse();

        Product returnedProduct = new ObjectMapper()
                .readValue(response.getContentAsByteArray(), Product.class);

        assertThat(returnedProduct.getId()).isEqualTo("10");
        assertThat(returnedProduct.getName()).isEqualTo("desk");
        assertThat(returnedProduct.getPrice()).isEqualTo(new BigDecimal("10.00"));
    }

    @Test
    public void createNewProductReturnsCreatedEntity() throws Exception {
        MockHttpServletResponse response = mockMvc
                .perform(post("/products")
                        .content("{\"id\":\"1\",\"name\":\"desk\",\"price\":\"10.00\"}")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn().getResponse();

        Product createdProduct = new ObjectMapper().readValue(response.getContentAsByteArray(), Product.class);
        assertThat(createdProduct.getId()).isEqualTo("1");
        assertThat(createdProduct.getName()).isEqualTo("desk");
    }

    @Test
    public void newProductsMustHavePrice() throws Exception {
        mockMvc.perform(post("/products")
                .content("{\"id\":\"1\",\"name\":\"desk\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andReturn().getResponse();
    }
}