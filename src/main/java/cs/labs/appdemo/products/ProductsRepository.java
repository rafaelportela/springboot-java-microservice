package cs.labs.appdemo.products;

import org.springframework.data.repository.CrudRepository;

interface ProductsRepository extends CrudRepository<Product, String> {
}
