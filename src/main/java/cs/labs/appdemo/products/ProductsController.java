package cs.labs.appdemo.products;

import io.micrometer.core.annotation.Timed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static com.google.common.collect.Lists.newArrayList;

@RestController
@RequestMapping("/products")
public class ProductsController {

    @Autowired
    private ProductsRepository productsRepository;

    private static final List<Product> ALL_PRODUCTS = newArrayList(
            new Product("10", "prod1", new BigDecimal("10.50")),
            new Product("11", "prod2", new BigDecimal("23.00")),
            new Product("12", "prod3", new BigDecimal("90.50")));

    @GetMapping
    @Timed("products.all")
    public Iterable<Product> allProducts() {
        return productsRepository.findAll();
    }

    @PostMapping
    @Timed("products.create")
    @ResponseStatus(code = HttpStatus.CREATED)
    public Product createProduct(@RequestBody @Valid Product newProduct) {
        return productsRepository.save(newProduct);
    }

    @GetMapping("/{id}")
    @Timed(value = "products.by-id")
    public Product getProduct(@PathVariable("id") String id) {

        Optional<Product> product = productsRepository.findById(id);

        if (product.isPresent())
            return product.get();
        else
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Product not found");
    }
}
